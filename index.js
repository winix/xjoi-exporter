const puppeteer = require('puppeteer');
const fs = require('fs');
const sleep = require('sleep');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const xjoi = require('./xjoi');

function save(problem, html) {
  const dom = new JSDOM(html);
  const node = dom.window.document.querySelector('div.divScroll > textarea');
  if(node) {
    var name;
    if (node.className == 'brush: delphi;')
      name = `${xjoi.dir}/${problem}.pas`;
    else
      name = `${xjoi.dir}/${problem}.cpp`;
    fs.writeFileSync(name, node.textContent);
    console.log(`${problem} saved.`);
  } else {
    console.log('Source of %s not found.', problem);
  }
}

(async () => {
  const browser = await puppeteer.launch({
    //slowMo: 250,
    defaultViewport: {
      width: 1280,
      height: 800
    }
  });
  const page = await browser.newPage();
  await page.goto(xjoi.base + '/');
  await page.click('li.dropdown:nth-child(6) > a:nth-child(1)');
  await page.type('#input-username', xjoi.username);
  await page.type('#input-password', xjoi.password);
  await page.click('#button-submit');
  await page.waitForSelector('.user-info');
  //await page.screenshot({path: 'home.png'});

  await page.goto(xjoi.base + '/user/profile/' + xjoi.username);
  await page.waitForSelector('.problem-view-main .view-body:nth-child(3)');
  await page.screenshot({path: 'profile.png'});

  const problemSelector = '.problem-view-main .view-body:nth-child(3) a';
  const problems = await page.evaluate(problemSelector => {
    const array = Array.from(document.querySelectorAll(problemSelector));
    return array.map(a => {
      return a.text;
    });
  }, problemSelector);
  console.log('Solved %d problems.', problems.length);

  fs.mkdirSync(xjoi.dir, {
    recursive: true
  });
  for (var i = 0; i < problems.length; i++) {
    if (i > 0)
      sleep.msleep(xjoi.sleep || 1000);
    var problem = problems[i];
    await page.goto(xjoi.base + `/status?pid=${problem}&user=${xjoi.username}&status=accepted&language=All`);
    await page.waitForSelector('th.status-table-title');
    var accepted = await page.$eval('a.status-accepted', el => el.href);
    var response = await page.goto(accepted);
    var html = await response.text();
    //console.log(html);
    save(problem, html);
  }

  await page.goto(xjoi.base + '/logout');
  //await page.screenshot({path: 'done.png'});

  await browser.close();
  console.log('Done');
})();
